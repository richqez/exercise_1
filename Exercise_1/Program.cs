﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{

    class Program
    {

        public static bool IsNumeric(String s)
        {
            int t;
            return int.TryParse(s, out t);
        }
        static void Main(string[] args)
        {
            int [,] mymoney =  new int[7,2]
            {
                {1000,0},
                {500,0},
                {100,0},
                {50,0},
                {10,0},
                {5,0},
                {1,0}
            };



            for (int i = 0; i < mymoney.GetLength(0); i++)
            {
                String billOrCoin =  (mymoney[i,0] > 10) ? "bill" : "coin";
                Console.Write(billOrCoin + " " + mymoney[i, 0] + "\t : ");
                var inputValue = Console.ReadLine();
                if (IsNumeric(inputValue))
                {
                    mymoney[i, 1] = int.Parse(inputValue);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("[error] is not numeric ");
                    Console.ResetColor();
                    i--;
                }
            }

            while (true)
            {
                
                Console.Write("input money \t :");
                int money = 0;
                while (true)
                {
                    var inputValue = Console.ReadLine();
                    if (IsNumeric(inputValue))
                    {
                        money = int.Parse(inputValue);
                        break;
                    }
                }

                int totalMoney = 0;
                for (int i = 0; i < mymoney.GetLength(0); i++)
                {
                    totalMoney += mymoney[i, 0] * mymoney[i, 1]; 
                }

                int[,] backup= new int[7, 2];
                Array.Copy(mymoney, backup,mymoney.Length);
                if (totalMoney < money)
                {
                    Console.WriteLine("not enough money");
                    continue;
                }
                    
                    int[] used = new int[mymoney.GetLength(0)];
                    bool isOk = true;
                    for (int i = 0; i < mymoney.GetLength(0); i++)
                    {
                        
                        if (mymoney[i, 0] == 0)
                            continue;

                        if ((mymoney[i, 1] - money / mymoney[i, 0]) >= 0)
                        {
                            used[i] = money / mymoney[i, 0] ;
                            mymoney[i, 1] -= used[i];
                            money = money % mymoney[i, 0];
                        }
                        else
                        {
                            money -= mymoney[i, 0] * mymoney[i, 1];
                            mymoney[i, 1] = 0;
                        }


                        if (i == mymoney.GetLength(0)-1)
                        {
                            if (money>0)
                            {
                                Console.WriteLine("not enough coin");
                                mymoney = backup;
                                isOk = false;
                                break;
                            }
                        }
                        
                    }

                if(isOk){
                    for (int i = 0; i < mymoney.GetLength(0); i++)
                    {
                        Console.WriteLine(mymoney[i, 0] + "\t used :" + used[i] + "\t remain: " + mymoney[i, 1]);
                    }
                }

            }

                Console.ReadLine();


        }
    }
}
